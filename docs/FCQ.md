Frequently Confused Questions
=============================


## Common Recommendations

1. **You should isolate workloads using namespaces**
    Theory: Kubernetes has new security features added to every release therefore you should upgrade to the latest version possible

    Qualifier:
    * Your hosted K8s server doesn’t support the latest version (AKS, EKS)
    * Upgrading would mean breaking everything else (plugins, storage providers, security tools)

    Practice: Upgrade to the latest versions your service provides
    * Make sure all CVE’s are patched
    * Try to employ the latest security controls
    Make sure it’s still a maintained version
    * K8s supports up to 3 minor versions
    * Minor release happen every 3 months
    * You must upgrade your cluster every 9 months!
    * This Monday, 1.11 will no longer be supported!




1. **You should have the latest version installed**
    Theory: Kubernetes has new security features added to every release therefore you should upgrade to the latest version possible
    Qualifier:
    * Your hosted K8s server doesn’t support the   latest version (AKS, EKS)
    * Upgrading would mean breaking everything else (plugins, storage providers, security tools)

    Practice: Upgrade to the latest versions your service provides
    * Make sure all CVE’s are patched
    * Try to employ the latest security controls
    Make sure it’s still a maintained version
    * K8s supports up to 3 minor versions
    * Minor release happen every 3 months
    * You must upgrade your cluster every 9 months!
    * This Monday, 1.11 will no longer be supported!



1. **You should use TLS to secure endpoints**
    Theory: You should protect all Kubernetes endpoints using mutual TLS.

    By default:
    * Certificates you generate for your users for kubectl cannot be revoked
    * Certificate Authority expires in 10 years

    Many Kubernetes environments will have 5+ separate certificate authorities!
    Practice: Deploying PKI is hard and it can’t be considered the same thing as deploying in an enterprise. 
    * Make sure Clusters are relatively ephemeral
    * Figure out how you can revoke access in your organization:
        * OpenID
        * Basic auth (bad but allows revocation!)



1. **Access controls should protect everything**
    Theory: You should enable RBAC on all of your clusters so all API requests go through an authorization check. 
    Absolute must but RBAC easily becomes complex
Easy to enable RBAC and then grant everyone admin access

    Practice:
    * Yes, enable RBAC. There are no reasons in a production cluster that tries to be secure to not.
    * Make sure roles reflect organization’s structure. 
        * Separate dev groups
        * Service tokens should be limited
        * Do your Pods need service tokens at all?
    * Decide how your deploy pipelines will use roles

1. **You should isolate workloads using Namespaces**
    Theory: A Namespace should provide isolation between tenants. 
    It doesn’t. Full stop.
    * If Tenant1 can run with normal permissions on the cluster, they can access Tentant2 resources 

    Kubernetes currently has no way of securely isolating multi-tenant environments. 

    The only true boundary is at the Cluster level

    Practice: Use namespaces as the beginning of your isolation controls
    * Leverage other controls (e.g. IAM policies) to control a namespace
    * Don’t rely on namespacing alone for anything security related
    * In the future, K8s plans to make namespacing a security boundary


1. **Sensitive workloads should be isolated**
    Theory: If you know a pod is going to be a high risk to the cluster, you should isolate it by:
    * Running on a dedicated Node
    * Running it in a Node that uses a hypervisor-based runtime
    * Running it in its own cluster

    Each of these solutions do effectively reduce the risk... but at a cost
    * A compromised node may be little barrier to compromising the rest of the cluster
    * Dedicated clusters can’t scale with the other services
    * Hypervisor runtimes aren’t yet mature to be relied upon
    Don’t forget the primary purpose of K8s



1. **Ensure that API and services aren't accessible to Pods**
    Theory: The metadata service can provide a lot of additional information about the cluster and the environment including access keys and could be used to pivot to other hosts.
    Absolutely true and one of the most common attack vectors.
    * Protect metadata service
    * Protect K8s API
    * Remove unnecessary network access
    Why is your etcd running on 0.0.0.0?

    Practice: Same as the theory. 
    At bare minimum make sure the metadata service isn’t exposed and that the K8s API requires additional authentication


1. **Use a Pod Security policy on every cluster**
    Theory: A Pod Security Policy provides a great way to set security expectations on all K8s objects and should be always used.
    PSPs are a beta feature of 1.13. 

    Admission Controller: 
    * Intercepts requests to the K8S API and applies authorization rules
    * When you apply a PSP, the admission controller reads it and checks that a Pod being deployed meets the requirements
    * If not, the API call will be rejected

    Practice: Yes we should do this but check if it’s even possible in the hosted environment. 
    Do you recommend a beta feature?
    Make PSP’s available across the cluster with varrying levels of protection. 
    * E.g. “Strict” PSP provides the highest security controls and “Minimal” is the organization’s baseline
    * If you can’t do PSPs:
    * Provide teams with service templates that show the expected level of security
    * Train devs to make their own security choices


1. **Make sure all nodes are hardened**
    Theory: A Node can provide a substantial amount of security controls that would prevent a breakout from a Pod. Therefore you should enable those controls.
    
    For sure. But do you know what does that mean?
    * Regularly updated and always getting the latest patches
    * Restricted runtime configuration (userns-remap?)
    * Do you need SSH to manage the systems?

    Practice: This is another personal decision of the organization. 
    Many monolithic controls worry about defending from external threats
    Nodes have a much higher risk of internal threats (breakouts)
    Look into alternatives like CoreOS that address all of these issues


1. **Use Network Policies**
    Background: Network Policies allow you fine grain control over communications 
    inside a Kubernetes cluster. 1.13 just started adding egress restrictions so 
    it's not always ideal.  
    
    You should have a network control that controls access to and from Pods, Nodes, and Namespaces. Up to you to decide if you can use a K8s network policy
    * IAM roles
    * Are you already using something like Istio that provides better restrictions?

    
1. **You should build you own internal Docker images**
    Theory: Building your container images from scratch ensures that nothing malicious can appear in the images
    No one wants to restrict developers to a single set of Docker images. 
    Practice: Restrict what gets deploy into production:
    Private container registries (ACR, ECR)
    * Vulnerability scanning images 
    * Build / Runtime scanning 
    * Third party tools (Twistlock)


1. **You sohuld audit all events in your cluster**
    Theory: Similar to monolithic thinking, auditing is the only way to be able to debug problems and respond to incidents in a cluster.

    Where to enable auditing:
    * K8S API - very important when the CSO wants  to know if you’re affected by the next CVE that comes out
    * Pod/Node - If all your Pods run as UID 0 (root), will you be able to know which accessed /etc/shadow?

    Practice: Monitoring and auditing is a must but K8s alone can only provide so much. 
    * Look at third party Pod auditing that can contextualize UID events
        * Sysdig Falco
        * go-audit
    * Enable a K8s audit policy on all clusters
    * Choose a monitoring solution that works with your organization structure


