![Emergency Kit Logo](img/kubernetes_first_aid.png)

# Emergency Kubernetes Security Kit

This kit has the basics for any Kubernetes environment if you're building a 
cluster and want to do it securely or if you're doing a security assessment 
and have to desperately figure out how Kubernetes works. 

* [@antitree](https://www.twitter.com/antitree)
* https://www.antitree.com/

This project is a reaction to friends, coworkers, and people on the Internet 
reaching out for help because they're in a perdicament. Either on a project
where the client says "I want a security assessment" but doesn't know they
have Kubernetes in their environment. 

**This should only be used in desperation.**

## Documentation

* [Frequently Confused Questions](/docs/FCQ.md)
* [Common Security Solutions](/docs/solutions.md)

## Tools
Tools you can use with blind desparation:

* [Aqua Kube-Hunter](https://github.com/aquasecurity/kube-hunter)
* [NCC Group Kube-Auto-Analyzer](https://github.com/nccgroup/kube-auto-analyzer)
* [Kubi-Scan](https://github.com/cyberark/KubiScan)

## Vendors
* Aqua
* Twistlock
* Istio
* Envoy
* INSERT YOUR VENDOR HERE


## Actual Learning Resources
In case you actually care about learning about Kubernetes long-term

* [Kubernetes Documentation](https://kubernetes.io/docs/home/)
* [Kubernetes The Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way)
* [A book on Kubernetes Security](https://kubernetes-security.info/)
* [Aqua Container Wiki](https://www.aquasec.com/wiki)

## Kubernetes Geniuses on the Internet
In case you think you can quickly befriend someone and ask them for their
opinion

* [@jessfraz (Jessie Frazelle)](https://twitter.com/jessfraz)
* [@liggitt (Jordan Liggitt)](https://twitter.com/liggitt?lang=en)
* [@raesene (Rory McCune)](https://twitter.com/raesene?lang=en)